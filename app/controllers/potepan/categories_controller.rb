class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes_price_and_images
    @taxonomies = Spree::Taxonomy.includes(:root)
  end
end
