class Potepan::ProductsController < ApplicationController
  MAXIMUM_NUMBERS_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products = Spree::Product.extract_related_products(@product).
      sample(MAXIMUM_NUMBERS_OF_RELATED_PRODUCTS)
  end
end
