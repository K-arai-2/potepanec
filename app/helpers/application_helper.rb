module ApplicationHelper
  SITE_TITLE = "BIGBAG store".freeze

  def full_title(page_title)
    return SITE_TITLE if page_title.blank?
    "#{page_title} - #{SITE_TITLE}"
  end
end
