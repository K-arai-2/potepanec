module Potepanec::ProductDecorator
  def self.prepended(base)
    base.class_eval do
      scope :includes_price_and_images, -> { includes(master: [:default_price, :images]) }

      def self.extract_related_products(product)
        in_taxons(product.taxons).
          includes_price_and_images.
          where.not(id: product.id).
          distinct
      end
    end
  end
  Spree::Product.prepend self
end
