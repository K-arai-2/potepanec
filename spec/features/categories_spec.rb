require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "categories") }
  let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxon1) { create(:taxon, name: "mugs", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: "clothing", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:product) { create(:product, name: "BAG", price: "10.00", taxons: [taxon]) }
  let!(:product1) { create(:product, name: "MUG", price: "20.00", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "SHIRT", price: "30.00", taxons: [taxon2]) }

  before do
    visit potepan_category_path(product.taxons.first.id)
  end

  scenario "the category#show page views well" do
    expect(page).to have_title "bags - BIGBAG store"
    expect(page).to have_selector 'h2', text: "bags"
    expect(page).to have_selector 'li.active', text: "bags"
    expect(page).to have_selector 'h3', text: "10.00"
    expect(page).to have_selector 'h5', text: "BAG"
    find("#category#{taxonomy.id}").click
    expect(page).to have_selector "#taxon#{taxon.id}", text: "bags (1)"
    expect(page).to have_selector "#taxon#{taxon1.id}", text: "mugs (1)"
    expect(page).to have_selector "#taxon#{taxon2.id}", text: "clothing (1)"
  end

  scenario "the taxon tag links its own category#show page" do
    find("#category#{taxonomy.id}").click
    find("#taxon#{taxon1.id}").click
    expect(current_path).to eq potepan_category_path(taxon1.id)
  end

  scenario "the product links its own product#show page" do
    click_on "BAG"
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
