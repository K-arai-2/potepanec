require 'rails_helper'
Capybara.ignore_hidden_elements = false

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let(:unrelated_taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let(:product) do
    create(:product, taxons: [taxon])
  end
  let(:no_description_product) { create(:product, description: nil, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let(:unrelated_product) { create(:product, taxons: [unrelated_taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "this product has no description" do
    visit potepan_product_path(no_description_product.id)
    expect(page).to have_selector 'p', text: "This product has no description"
  end

  scenario "the show page views well" do
    expect(page).to have_title "#{product.name} - BIGBAG store"
    expect(page).to have_selector 'h2', text: product.name
    expect(page).to have_selector 'li.active', text: product.name
    expect(page).to have_selector 'h3', text: product.price.to_s
    expect(page).to have_selector 'p', text: product.description
    expect(page).to have_selector 'h5', text: related_product.name
    expect(page).not_to have_selector 'h5', text: product.name
    expect(page).not_to have_selector 'h5', text: unrelated_product.name
  end

  scenario "the brand link works well" do
    click_link_to_home('.navbar-brand')
  end

  scenario "the navbar link works well" do
    click_link_to_home('.nav-link')
  end

  scenario "the breadcrum link works well" do
    click_link_to_home('.bread-link')
  end

  scenario "the back to the category#show link works well" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario "the related_items product#show link works well" do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end

  def click_link_to_home(css_class)
    find(css_class).click
    expect(current_path).to eq potepan_path
  end
end
