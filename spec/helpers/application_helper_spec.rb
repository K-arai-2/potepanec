require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    subject { full_title(page_title) }

    context "with page_title" do
      let(:page_title) { create(:base_product, name: "BAG").name }

      it { expect(subject).to eq "BAG - BIGBAG store" }
    end

    context "without page_title" do
      let(:page_title) { "" }

      it { expect(subject).to eq "BIGBAG store" }
    end

    context "with nil title" do
      let(:page_title) { nil }

      it { expect(subject).to eq "BIGBAG store" }
    end
  end
end
