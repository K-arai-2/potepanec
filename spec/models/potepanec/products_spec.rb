require 'rails_helper'

RSpec.describe Potepanec::ProductDecorator, type: :model do
  describe 'extract_related_products' do
    subject do
      Spree::Product.extract_related_products(product).ids
    end

    let(:category) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: category, parent: category.root) }
    let(:other_taxon) { create(:taxon, taxonomy: category, parent: category.root) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }

    it { is_expected.not_to eq [product.id] }
    it { is_expected.not_to eq [unrelated_product.id] }
    it { is_expected.to eq [related_product.id] }
  end
end
