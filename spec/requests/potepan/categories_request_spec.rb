require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy, name: "categories") }
    let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
    let(:product) { create(:product, name: "BAG", price: "20.00", taxons: [taxon]) }
    let(:another_taxon) { create(:taxon, name: "clothing", parent: taxonomy.root, taxonomy: taxonomy) }
    let(:another_product) { create(:product, name: "SHIRT", price: "100.00", taxons: [another_taxon]) }

    before do
      get potepan_category_path(product.taxon_ids)
    end

    it { expect(response).to be_successful }
    it { expect(response).to have_http_status "200" }
    it { expect(response.body).to include "BAG" }
    it { expect(response.body).to include "20.00" }
    it { expect(response.body).to include "categories" }
    it { expect(response.body).to include 'bags' }
    it { expect(response.body).not_to include "SHIRT" }
    it { expect(response.body).not_to include "100.00" }
    it { expect(response.body).not_to include "clothing" }
  end
end
