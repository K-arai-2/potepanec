require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "#show" do
    let(:maximum_numbers_of_related_products) { 4 }
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it { expect(response).to be_successful }
    it { expect(response).to have_http_status "200" }
    it { expect(response.body).to include product.name }
    it { expect(response.body).to include product.price.to_s }
    it { expect(response.body).to include "#{product.name} - BIGBAG store" }
    it { expect(assigns(:related_products).size).to eq maximum_numbers_of_related_products }
  end
end
